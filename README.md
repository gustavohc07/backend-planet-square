# PlanetSquare

## Como rodar o projeto

O projeto em questão trata-se apenas do backend e utiliza o banco MySQL. Para vê-lo em funcionamento é necessário que inicialize o frontend para consumir a API
ou entao utilizar algum programa que realize requisições, como o Postman.

Para utilizar o frontend acesse [Frontend](https://gitlab.com/gustavohc07/frontend-planet-square), clone o projeto e inicialize o servidor
com o seguinte comando:

``` ./mvnw spring-boot:run ```

Como backend rodando, basta executar o comando ng `ng serve` no frontend e acessar 
`http://localhost:4200/`.

## Funcionalidades

A API fornece o CRUD completo de um objeto. As rotas são as seguintes:

`GET http//localhost:8080/planets` Lista todos os planetas

`GET http//localhost:8080/planets/:id` Busca por um planeta em específico

`POST http//localhost:8080/planets/new` Cria um novo planeta

`PUT http://localhost:8080/planets/:id/edit` Edita um planeta

`DELETE http://localhost:8080/planets/:id` Deleta um planeta

