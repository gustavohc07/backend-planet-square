package com.codesquare.planetapi.controller;

import com.codesquare.planetapi.model.Planet;
import com.codesquare.planetapi.repository.PlanetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/planets")
@CrossOrigin(origins = "*")
public class PlanetController {
    Map<String, Planet> planets;

    @Autowired
    private PlanetRepository planetRepository;

    @GetMapping
    public Collection<Planet> all() {
        return planetRepository.findAll();
    }

    @PutMapping("{id}/edit")
    public ResponseEntity<Planet> updatePlanet(@PathVariable("id") Long id, @RequestBody Planet planetDetails) throws ResourceNotFoundException {
        Planet planet = planetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Planet not found"));

        planet.setName(planetDetails.getName());
        planet.setDescription(planetDetails.getDescription());
        planet.setImage_url(planetDetails.getImage_url());
        planet.setRadius(planetDetails.getRadius());
        planet.setWater(planetDetails.getWater());
        planet.setDiscovery(planetDetails.getDiscovery());

        final Planet updatedPlanet = planetRepository.save(planet);
        return ResponseEntity.ok(updatedPlanet);
    }

    @GetMapping("{id}")
    public ResponseEntity<Planet> find(@PathVariable Long id) {
        Optional<Planet> planet = planetRepository.findById(id);
        if (planet.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(planet.get());
    }

    @PostMapping("/new")
    @ResponseBody
    public String addNewPlanet(@RequestBody Planet planet) {
        planetRepository.save(planet);
        return "OK";
    }


    @DeleteMapping("{id}")
    public String deletePlanet(@PathVariable Long id) {
        Optional<Planet> planet = planetRepository.findById(id);
        if(planet.isPresent()) {
            planetRepository.delete(planet.get());
            return "Planet deleted";
        } else {
            throw new RuntimeException("Planet not found");
        }
    }
}