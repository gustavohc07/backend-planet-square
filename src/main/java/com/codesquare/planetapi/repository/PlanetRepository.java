package com.codesquare.planetapi.repository;

import com.codesquare.planetapi.model.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PlanetRepository extends JpaRepository<Planet, Long> {
}
